cluster = [
    {:host=>"db", :ip=>"192.168.56.101", :ram=>512, :shared=>false, :aliases=>[]},
    {:host=>"web", :ip=>"192.168.56.102", :ram=>1024, :shared=>true, :aliases=>[]},
    {:host=>"cache", :ip=>"192.168.56.103", :ram=>512, :shared=>false, :aliases=>[]},
    {:host=>"queue", :ip=>"192.168.56.104", :ram=>512, :shared=>false, :aliases=>[]},
];

def process_aliases(domain, aliases)
  aliases = aliases.map { |host| host + domain }
  
  return aliases
end

Vagrant.configure("2") do |config|
  local_domain = ".darwinsoft.dev"

  config.vm.box = "ubuntu/trusty64"
  
  config.ssh.forward_agent = true
  config.ssh.insert_key = false
  
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.include_offline = true
  
  cluster.each_with_index do |vm, index|
    config.vm.define vm[:host] do |server|
      hostname = vm[:host] + local_domain
      
      if vm[:shared]
        if Vagrant.has_plugin?("vagrant-bindfs")
          server.vm.synced_folder "./shared/" + vm[:host], "/vhosts-nfs", :nfs=>true
          config.bindfs.bind_folder "/nfs-vhosts", "/vhosts", group: "www-data", perms: "u=rwx:g=rwx:o=rx", chwon_ignore: true, chmod_ignore: true
        else
          server.vm.synced_folder "./shared/" + vm[:host], "/vhosts", :nfs=>true
        end
      end
      
      server.vm.hostname = hostname
      server.vm.network :private_network, ip: vm[:ip]
      
      server.vm.provider :virtualbox do |v|
        v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        v.customize ["modifyvm", :id, "--name", hostname]
        v.customize ["modifyvm", :id, "--memory", vm[:ram].to_s]
      end
      
      server.hostmanager.aliases = process_aliases(local_domain, vm[:aliases])
      
      if index == cluster.size - 1
        server.vm.provision :ansible do |ansible|
          ansible.limit = "all"
          
          ansible.inventory_path = "ansible/inventory"
          ansible.playbook = "ansible/playbook.yml"
        end
      end
    end
  end
end
